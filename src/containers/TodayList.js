import React from 'react';
import LongPress from 'react-long'
import Icon24Like from '@vkontakte/icons/dist/24/like';
import Icon24LikeOutline from '@vkontakte/icons/dist/24/like_outline';
import Icon24ShareOutline from '@vkontakte/icons/dist/24/share_outline';
import Icon24CommentOutline from '@vkontakte/icons/dist/24/comment_outline';
import Icon28DeleteOutline from '@vkontakte/icons/dist/28/delete_outline';
import { Avatar, Alert } from '@vkontakte/vkui';

function TodayList(props) {
        let todayElements
        if (props.data.length > 0) {
                todayElements = props.data.map(item => {
                        return <div key={item._id} className='Today' style={{ backgroundImage: item.background }}>
                                <Avatar className='Today__avatar' src={item.user_avatar} onClick={() => props.getUserOpen({ id: item.user_id })} />
                                <div className='Today__title'>
                                        <div>{decodeURIComponent(item.user_name)} {item.when ? item.when : 'сегодня'}</div>
                                        <div>{item.title}</div>
                                </div>
                                <div className='Today__detail'>{item.description}</div>
                                <div className='Today__bottom'>
                                        <LongPress
                                                time={666}
                                                onLongPress={() => item.likes > 0 ? props.openLikes(item._id) : {}}
                                                onPress={() => props.toggleLike(item._id)}
                                        >
                                                <div className='Today__bottom_icon'>{item.liked ? <Icon24Like /> : <Icon24LikeOutline />}<span>{item.likes}</span></div>
                                        </LongPress>
                                        <div className='Today__bottom_icon' onClick={() => props.openComment(item._id)}><Icon24CommentOutline /><span>{item.comments}</span></div>
                                        <div className='Today__bottom_icon' onClick={() => props.shareBtn(item)}><Icon24ShareOutline /></div>
                                        {item.author && <div className='Today__bottom_icon' onClick={() => props.setPopout(<Alert
                                                actionsLayout="vertical"
                                                actions={[{
                                                        title: 'Удалить',
                                                        autoclose: true,
                                                        mode: 'destructive',
                                                        action: () => props.deleteToday(item._id),
                                                }, {
                                                        title: 'Отмена',
                                                        autoclose: true,
                                                        mode: 'cancel'
                                                }]}
                                                onClose={() => props.setPopout(null)}
                                        >
                                                <h2>Подтвердите действие</h2>
                                        </Alert>)}><Icon28DeleteOutline width={24} height={24} /></div>}
                                </div>
                        </div>
                })
        }
        return (
                <div>
                        {todayElements}
                </div>
        )

}

export default TodayList;