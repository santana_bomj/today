import React from 'react';
import { Group, Cell, List, Avatar, Placeholder } from '@vkontakte/vkui';
import Icon24LikeOutline from '@vkontakte/icons/dist/24/like_outline';
import Icon56UsersOutline from '@vkontakte/icons/dist/56/users_outline';
function RatingList(props) {
        let ratingElements = props.type !== 'friends' ? <Placeholder
                icon={<Icon56UsersOutline />}
        >
                Рейтинг ещё не сформирован
        </Placeholder> : <Placeholder
                        icon={<Icon56UsersOutline />}
                >
                        Нет друзей
                </Placeholder>
        if (props.data.length !== 0) {
                ratingElements = props.data.map(item => {
                        return <React.Fragment key={item._id}>
                                <Cell onClick={item._id ? () => props.getUserOpen({ id: item._id }) : () => { }} before={<Avatar size={40} src={decodeURIComponent(item.avatar)} />} indicator={<div className='Rating'><span>{item.rating}</span><Icon24LikeOutline /></div>} >{decodeURIComponent(item.name)}</Cell>
                        </React.Fragment>
                })
        }
        return (
                <Group>
                        <List>
                                {ratingElements}
                        </List>
                </Group>
        )

}

export default RatingList;