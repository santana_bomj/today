import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Panel, PanelHeader, Div, Spinner, Footer, Avatar, Placeholder, PanelHeaderBack } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import * as mainSelectors from "../store/main/reducer";
import * as mainActions from '../store/main/actions';
import * as todaySelectors from "../store/today/reducer";
import * as todayActions from '../store/today/actions';
import helpers from '../services/helpers'

import Icon56UsersOutline from '@vkontakte/icons/dist/56/users_outline';

import TodayList from './TodayList'

class ProfilePanel extends Component {
    constructor(props) {
        super(props);
        this.handleScroll = this.handleScroll.bind(this);
    }
    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
    }

    handleScroll() {
        let offset = 200
        let windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
        let body = document.body;
        let html = document.documentElement;
        let docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
        let windowBottom = windowHeight + window.pageYOffset;
        if (windowBottom >= docHeight - offset && this.props.isBottomLoaded && this.props.isProfileLoaded) {
            this.props.appendProfileData()
        }
    }
    render() {
        return (
            <Panel id={this.props.id}>
                <PanelHeader left={this.props.route.name === "profileuser" && <PanelHeaderBack onClick={() => window.history.go(-1)} />}>
                    Профиль
                </PanelHeader>
                <Div>
                    {this.props.activeUserLoading ?
                        <div className='Spinner__margin'><Spinner size="regular" /></div>
                        : <>
                            {
                                this.props.user && <div className='Today Today__profile' style={{ backgroundImage: "linear-gradient(167deg, #928358, #c61084)" }}>
                                    <Avatar className='Today__avatar' src={decodeURIComponent(this.props.user.avatar)} />
                                    <div className='Today__title'>
                                        <div>{decodeURIComponent(this.props.user.name)}</div>
                                    </div>
                                    <div className='Today__detail'>
                                        <div>{decodeURIComponent(this.props.user.likes_sum) + ' ' + helpers.declOfNum(this.props.user.likes_sum, ['лайк', 'лайка', 'лайков'])}</div>
                                        <div>{decodeURIComponent(this.props.user.today_sum) + ' ' + helpers.declOfNum(this.props.user.today_sum, ['пост', 'поста', 'постов'])}</div>
                                    </div>
                                </div>
                            }
                            {this.props.user === 'none' && <Placeholder
                                icon={<Icon56UsersOutline />}
                                stretched
                            >
                                Пользователь не найден
                    </Placeholder>}
                            <TodayList data={this.props.data} setPopout={this.props.setPopout} deleteToday={this.props.deleteToday} toggleLike={this.props.toggleLike} getData={this.props.getData} shareBtn={this.props.shareBtn} openLikes={this.props.openLikes} openComment={this.props.openComment} />
                            {!this.props.isAllLoaded ? <div className='Spinner__margin'><Spinner size="regular" /></div> : <Footer className='Footer__without-margin'>Нет новых записей</Footer>}
                        </>
                    }
                </Div>
                {this.props.snackbar}
            </Panel >
        );
    }
}

function mapStateToProps(state) {
    return {
        data: todaySelectors.getProfileData(state),
        sLoaded: todaySelectors.isProfileLoaded(state),
        isBottomLoaded: todaySelectors.isProfileBottomLoaded(state),
        isAllLoaded: todaySelectors.isProfileAllLoaded(state),
        isProfileLoaded: todaySelectors.isProfileLoaded(state),
        user: mainSelectors.getUser(state),
        activeUserLoading: mainSelectors.getActiveUserLoading(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        shareBtn: (from) => dispatch(mainActions.shareBtn(from)),
        appendProfileData: () => dispatch(todayActions.appendProfileData()),
        toggleLike: (_id) => dispatch(todayActions.toggleLike(_id)),
        openLikes: (_id) => dispatch(todayActions.openLikes(_id)),
        openComment: (_id) => dispatch(todayActions.openComment(_id)),
        setPopout: (value) => dispatch(mainActions.setPopout(value)),
        deleteToday: (_id) => dispatch(todayActions.deleteToday(_id)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePanel);
