import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Avatar } from '@vkontakte/vkui';
import TextareaAutosize from 'react-autosize-textarea';
import Icon24Send from '@vkontakte/icons/dist/24/send';

import * as mainSelectors from "../store/main/reducer";
import * as todaySelectors from "../store/today/reducer";
import * as todayActions from '../store/today/actions';

import Gradients from './Gradients'

var classNames = require('classnames');

class AddTodayPanel extends Component {
    render() {
        return (
            <div className='Today' style={{ backgroundImage: this.props.active }}>
                <Avatar className='Today__avatar' src={this.props.urlArray.avatar} />
                <div className='Today__title'>
                    <div>Я сегодня</div>
                    <TextareaAutosize maxLength='128' name='title' value={this.props.title} onChange={this.props.handleInput} className='Today__title_textarea' placeholder='Введите своё событие...' />
                </div>
                <div className='Today__detail'>
                    <TextareaAutosize maxLength='1028' name='description' value={this.props.description} onChange={this.props.handleInput} className='Today__detail_textarea' placeholder='Опишите подробнее...' />
                </div>
                <div className='Today__bottom'>
                    <Gradients gradientsList={this.props.gradientsList} appendGradients={this.props.appendGradients} selectGradient={this.props.selectGradient} />
                    <div className={classNames('Today__bottom_icon', this.props.title === '' ? 'disable' : '' )} onClick={() => this.props.sendToday()}><Icon24Send /></div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        urlArray: mainSelectors.getUrlArray(state),
        active: todaySelectors.getActive(state),
        gradientsList: todaySelectors.getGradientsList(state),
        title: todaySelectors.getTitle(state),
        description: todaySelectors.getDescription(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        appendGradients: () => dispatch(todayActions.appendGradients()),
        selectGradient: (item) => dispatch(todayActions.selectGradient(item)),
        handleInput: (e) => dispatch(todayActions.handleInput(e)),
        sendToday: () => dispatch(todayActions.sendToday()),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(AddTodayPanel);
