import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ConfigProvider, Epic, Tabbar, TabbarItem, View } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import * as mainSelectors from '../store/main/reducer';
import * as vkSelectors from '../store/vk/reducer';
import * as vkActions from '../store/vk/actions';
import * as mainActions from '../store/main/actions';
import StartPanel from './StartPanel';
import MainPanel from './MainPanel';
import ProfilePanel from './ProfilePanel';
import RatingPanel from './RatingPanel';
import { RouteNode } from 'react-router5'
import Icon28Profile from '@vkontakte/icons/dist/28/profile';
import Icon28FavoriteOutline from '@vkontakte/icons/dist/28/favorite_outline';
import Icon28DevicesOutline from '@vkontakte/icons/dist/28/devices_outline';

import ModalContainer from './Modals/ModalContainer';

class App extends Component {

    componentDidMount() {
        document.body.attributes.scheme.value = 'space_gray'
        this.props.dispatch(vkActions.initApp());
        this.props.dispatch(mainActions.initalMain());
        window.addEventListener('popstate', e => { e.preventDefault(); this.props.dispatch(mainActions.goBack()); });
    }

    render() {
        let activePanel = 'main'
        let activeStory
        let tabbar
        switch (this.props.route.name) {
            case 'start':
                activeStory = 'start'
                tabbar = null
                break
            case 'main':
                activeStory = 'main'
                break
            case 'profile':
                activeStory = 'profile'
                break
            case 'profileuser':
                activeStory = 'profile'
                break            
            case 'rating':
                activeStory = 'rating'
                break           
            default:
                activeStory = 'start'
                break
        }

        if (tabbar !== null)
            tabbar = <Tabbar>
                <TabbarItem
                    selected={activeStory === 'profile'}
                    onClick={() => {
                        this.props.router.navigate('profile', {}, { replace: true })
                    }}><Icon28Profile /></TabbarItem>
                <TabbarItem
                    onClick={() => {
                        this.props.router.navigate('main', {}, { replace: true })
                    }}
                    selected={activeStory === 'main'}
                ><Icon28DevicesOutline /></TabbarItem>
                <TabbarItem
                    onClick={() => {
                        this.props.router.navigate('rating', {}, { replace: true })
                    }}
                    selected={activeStory === 'rating'}><Icon28FavoriteOutline /></TabbarItem>
            </Tabbar>

        return (
            <ConfigProvider insets={this.props.insets} >
                <Epic activeStory={activeStory} tabbar={tabbar}>
                    <View id="start" activePanel="main" popout={this.props.popout} modal={<ModalContainer />}>
                        <StartPanel router={this.props.router} id="main" />
                    </View>
                    <View id="main" activePanel={activePanel} popout={this.props.popout} modal={<ModalContainer />}>
                        <MainPanel router={this.props.router} id="main" />
                    </View>
                    <View id="profile" activePanel={activePanel} popout={this.props.popout} modal={<ModalContainer name={this.props.route.name} />}>
                        <ProfilePanel router={this.props.router} route={this.props.route} id="main" />
                    </View>
                    <View id="rating" activePanel="main" popout={this.props.popout}>
                        <RatingPanel router={this.props.router} id="main" />
                    </View>
                </Epic>
            </ConfigProvider>
        );
    }
}

function mapStateToProps(state) {
    return {
        popout: mainSelectors.getPopout(state),
        insets: vkSelectors.getInsets(state),
    };
}

export default connect(mapStateToProps)(
    (props) => (
        <RouteNode nodeName="">
            {({ route }) => <App route={route} {...props} />}
        </RouteNode>
    )
);
