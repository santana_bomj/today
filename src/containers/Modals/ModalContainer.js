import React from 'react';
import { connect } from 'react-redux';
import { ModalRoot, ModalCard, Avatar, Button, PanelHeaderButton, ModalPage, ModalPageHeader, IS_PLATFORM_ANDROID, IS_PLATFORM_IOS, Spinner, List, Cell, Textarea, Footer } from '@vkontakte/vkui';
import * as mainSelectors from "../../store/main/reducer";
import * as mainActions from '../../store/main/actions';
import * as todaySelectors from "../../store/today/reducer";
import * as todayActions from '../../store/today/actions';
import * as vkActions from '../../store/vk/actions';

import Icon56ErrorOutline from '@vkontakte/icons/dist/56/error_outline';
import Icon56CheckCircleOutline from '@vkontakte/icons/dist/56/check_circle_outline';
import Icon56ServicesOutline from '@vkontakte/icons/dist/56/services_outline';
import Icon24Cancel from '@vkontakte/icons/dist/24/cancel';
import Icon24Dismiss from '@vkontakte/icons/dist/24/dismiss';
import Icon24Send from '@vkontakte/icons/dist/24/send';
import Icon56LikeOutline from '@vkontakte/icons/dist/56/like_outline';

var classNames = require('classnames');

class ModalContainer extends React.Component {
    componentDidUpdate() {
        if (this.props.activeModal)
            this.props.setPath()
    }
    render() {
        return (
            <ModalRoot activeModal={this.props.activeModal}>
                <ModalCard
                    id='share'
                    icon={<Icon56ServicesOutline />}
                    header="Поделиться"
                    caption=" "
                    onClose={() => {
                        this.props.clearCanvas()
                        this.props.modalBack()
                    }}
                    actions={([{
                        title: 'В истории',
                        mode: 'primary',
                        action: () => this.props.share()
                    }, {
                        title: 'Закрыть',
                        mode: 'secondary',
                        action: () => this.props.modalBack()
                    }])}
                    actionsLayout="vertical"
                >
                    <div id='Today__share' className='Today Today__share' style={{ backgroundImage: this.props.activeToday.background }}>
                        <Avatar className='Today__avatar' src={this.props.activeToday.user_avatar} />
                        <div className='Today__title'>
                            <div>{this.props.activeToday.user_name} {this.props.activeToday.when ? this.props.activeToday.when : 'сегодня'}</div>
                            <div>{this.props.activeToday.title}</div>
                        </div>
                        <div className='Today__detail'>{this.props.activeToday.description}</div>
                        <div className='Today__bottom'>
                            <Button mode='overlay_primary'>А что ты сделал сегодня?</Button>
                        </div>
                    </div>
                </ModalCard>
                <ModalCard
                    id='story_token'
                    icon={<Icon56LikeOutline />}
                    header="Лайки с истории"
                    caption="Разрешите доступ к историям, чтобы мы могли сложить лайки с историй с лайками в карточке"
                    onClose={() => this.props.modalBack()}
                    actions={([{
                        title: 'Разрешить',
                        mode: 'primary',
                        action: () => this.props.VKWebAppGetAuthToken('stories')
                    }, {
                        title: 'Закрыть',
                        mode: 'secondary',
                        action: () => this.props.modalBack()
                    }])}
                    actionsLayout="vertical"
                />
                <ModalCard
                    id='error'
                    icon={<Icon56ErrorOutline />}
                    header="Ошибка"
                    caption={this.props.msg_error}
                    onClose={() => this.props.modalBack()}
                    actions={[{
                        title: 'Закрыть',
                        mode: 'primary',
                        action: () => this.props.modalBack()
                    }]}
                />
                <ModalCard
                    id='true'
                    icon={<Icon56CheckCircleOutline />}
                    header="Успешно"
                    caption={this.props.msg_error}
                    onClose={() => this.props.modalBack(2)}
                    actions={[{
                        title: 'Закрыть',
                        mode: 'primary',
                        action: () => this.props.modalBack(2)
                    }]}
                />
                <ModalPage
                    id='likes'
                    header={
                        <ModalPageHeader
                            left={IS_PLATFORM_ANDROID && <PanelHeaderButton onClick={() => {
                                this.props.modalBack()
                                this.props.closeLikes()
                            }}><Icon24Cancel /></PanelHeaderButton>}
                            right={IS_PLATFORM_IOS && <PanelHeaderButton onClick={() => {
                                this.props.modalBack()
                                this.props.closeLikes()
                            }}><Icon24Dismiss /></PanelHeaderButton>}
                        >
                            Лайки
                        </ModalPageHeader>
                    }
                    settlingHeight={100}
                    dynamicContentHeight={true}
                    onClose={() => {
                        this.props.modalBack()
                        this.props.closeLikes()
                    }}
                >
                    {!this.props.likesLoaded ? <div className='Spinner__modal'><div><Spinner size="regular" /></div></div> :
                        <List>
                            {this.props.likes && this.props.likes.map((item) => {
                                return (
                                    <Cell
                                        before={<Avatar src={decodeURIComponent(item.avatar)} />}
                                        key={item._id}
                                    >{decodeURIComponent(item.name)}</Cell>
                                );
                            })}
                        </List>}
                </ModalPage>
                <ModalPage
                    id='comments'
                    header={
                        <ModalPageHeader
                            left={IS_PLATFORM_ANDROID && <PanelHeaderButton onClick={() => {
                                this.props.modalBack()
                                this.props.closeComment()
                            }}><Icon24Cancel /></PanelHeaderButton>}
                            right={IS_PLATFORM_IOS && <PanelHeaderButton onClick={() => {
                                this.props.modalBack()
                                this.props.closeComment()
                            }}><Icon24Dismiss /></PanelHeaderButton>}
                        >
                            Комментарии
                        </ModalPageHeader>
                    }
                    settlingHeight={100}
                    dynamicContentHeight={true}
                    onClose={() => {
                        this.props.modalBack()
                        this.props.closeComment()
                    }}
                >
                    {!this.props.commentsLoaded ? <Spinner size="regular" /> :
                        <List>
                            {this.props.comments && this.props.comments.length > 0 ? this.props.comments.map((item) => {
                                let date = new Date(item.ts)
                                var now = new Date(Date.now())
                                var daysLag = Math.ceil(Math.abs(now.getTime() - date.getTime()) / (1000 * 3600 * 24))
                                let when
                                if (daysLag === 0)
                                    when = ', сегодня'
                                else if (daysLag === 1)
                                    when = ', вчера'
                                else
                                    when = ' ' + date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear()
                                let time = date.getHours() + ':' + date.getMinutes()
                                return (
                                    <Cell
                                        className='Comments'
                                        before={<Avatar src={decodeURIComponent(item.avatar)} />}
                                        key={item._id}
                                        size="l"
                                        description={<div className='Comments__message'>{decodeURIComponent(item.message)}</div>}
                                        bottomContent={<div className='Comments__ts'>{time + when}</div>}
                                    >{decodeURIComponent(item.name)}</Cell>
                                );
                            }) : <Footer className='Footer__without-margin'>Оставьте первый комментарий</Footer>}
                        </List>}
                    <div className='Comment'>
                        <Textarea className='Comment__input' name='comment' value={this.props.comment} onChange={this.props.handleInput} placeholder='Комментарий' />
                        {this.props.commentLoading ? <div className='Comment__send'><Spinner size="regular" /></div> : <Icon24Send className={classNames('Comment__send', this.props.comment === '' ? 'disable' : '')} onClick={() => (this.props.comment !== '' ? this.props.sendComment() : {})} />}
                    </div>
                    <div className='Indent' />
                </ModalPage>
            </ModalRoot>
        )
    }
}

function mapStateToProps(state) {
    return {
        activeModal: mainSelectors.getActiveModal(state),
        msg_error: mainSelectors.getMsgError(state),
        activeToday: todaySelectors.getActiveToday(state),
        likes: todaySelectors.getLikes(state),
        likesLoaded: todaySelectors.isLikesLoaded(state),
        comment: todaySelectors.getComment(state),
        comments: todaySelectors.getComments(state),
        commentsLoaded: todaySelectors.isCommentsLoaded(state),
        commentLoading: todaySelectors.getCommentLoading(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setActiveModal: (activeModal) => dispatch(mainActions.setActiveModal(activeModal)),
        modalBack: (value) => dispatch(mainActions.modalBack(value)),
        setPath: () => dispatch(mainActions.setPath()),
        share: () => dispatch(mainActions.share()),
        closeLikes: () => dispatch(todayActions.closeLikes()),
        handleInput: (e) => dispatch(todayActions.handleInput(e)),
        sendComment: () => dispatch(todayActions.sendComment()),
        closeComment: () => dispatch(todayActions.closeComment()),
        VKWebAppGetAuthToken: (scope) => dispatch(vkActions.VKWebAppGetAuthToken(scope)),
        clearCanvas: () => dispatch(mainActions.clearCanvas()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalContainer);