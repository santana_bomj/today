import React from 'react';
var classNames = require('classnames');

function handleScroll(e, action) {
  if (e.target.offsetWidth + e.target.scrollLeft === e.target.scrollWidth) {
    action()
  }
}

function Gradients(props) {
  let gradientsList = props.gradientsList.map(item => {
    return <div key={item._id} className={classNames('Gradient__item', props.active === item.gradient ? 'active' : '')} onClick={() => props.selectGradient(item.gradient)}>
      <div className='Gradient__item_background' style={{ backgroundImage: item.gradient }} />
    </div>
  })
  return (
    <div className='Gradient' onScroll={(e) => handleScroll(e, props.appendGradients)}>
      {gradientsList}
    </div>
  )
}
export default Gradients;
