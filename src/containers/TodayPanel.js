import React, { Component } from 'react';
import { connect } from 'react-redux';
import { PullToRefresh, Spinner, Footer } from '@vkontakte/vkui';

import * as mainActions from '../store/main/actions';
import * as todaySelectors from "../store/today/reducer";
import * as todayActions from '../store/today/actions';

import TodayList from './TodayList'

class TodayPanel extends Component {
    constructor(props) {
        super(props);
        this.handleScroll = this.handleScroll.bind(this);
    }
    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
    }

    handleScroll() {
        let offset = 600
        let windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
        let body = document.body;
        let html = document.documentElement;
        let docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
        let windowBottom = windowHeight + window.pageYOffset;
        if (windowBottom >= docHeight - offset && this.props.isBottomLoaded) {
            this.props.appendData()
        }
    }

    render() {
        return (<>
            <PullToRefresh onRefresh={this.props.getData} isFetching={!this.props.isLoaded}>
                <TodayList data={this.props.data} getUserOpen={this.props.getUserOpen} setPopout={this.props.setPopout} deleteToday={this.props.deleteToday} toggleLike={this.props.toggleLike} getData={this.props.getData} shareBtn={this.props.shareBtn} openLikes={this.props.openLikes} openComment={this.props.openComment} />
                {!this.props.isAllLoaded ? <div className='Spinner__margin'><Spinner size="regular" /></div> : <Footer className='Footer__without-margin'>За сегодня больше нет новых записей</Footer>}
            </PullToRefresh>
        </>
        );
    }
}

function mapStateToProps(state) {
    return {
        title: todaySelectors.getTitle(state),
        description: todaySelectors.getDescription(state),
        data: todaySelectors.getData(state),
        isLoaded: todaySelectors.isLoaded(state),
        isBottomLoaded: todaySelectors.isBottomLoaded(state),
        isAllLoaded: todaySelectors.isAllLoaded(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        appendGradients: () => dispatch(todayActions.appendGradients()),
        selectGradient: (item) => dispatch(todayActions.selectGradient(item)),
        handleInput: (e) => dispatch(todayActions.handleInput(e)),
        toggleLike: (_id) => dispatch(todayActions.toggleLike(_id)),
        getData: () => dispatch(todayActions.getData()),
        appendData: () => dispatch(todayActions.appendData()),
        shareBtn: (item) => dispatch(mainActions.shareBtn(item)),
        openLikes: (_id) => dispatch(todayActions.openLikes(_id)),
        openComment: (_id) => dispatch(todayActions.openComment(_id)),
        setPopout: (value) => dispatch(mainActions.setPopout(value)),
        deleteToday: (_id) => dispatch(todayActions.deleteToday(_id)),
        getUserOpen: (params) => dispatch(mainActions.getUserOpen(params)),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(TodayPanel);
