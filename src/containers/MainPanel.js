import React, { Component } from "react";
import { connect } from "react-redux";
import { Panel, PanelHeader, Div } from "@vkontakte/vkui";
import "@vkontakte/vkui/dist/vkui.css";
import "./style.scss";
import * as mainSelectors from "../store/main/reducer";
import * as mainActions from '../store/main/actions';

import AddTodayPanel from "./AddTodayPanel";
import TodayPanel from "./TodayPanel";


class MainPanel extends Component {
  render() {
    return (
      <Panel id={this.props.id}>
        <PanelHeader>Я сегодня</PanelHeader>

        <Div>
          <AddTodayPanel />
          <TodayPanel />
        </Div>
        {this.props.snackbar}
      </Panel >
    );
  }
}

function mapStateToProps(state) {
  return {
    snackbar: mainSelectors.getSnackbar(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {    
    shareBtn: (id) => dispatch(mainActions.shareBtn(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainPanel);
