import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Alert } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import { closePopout } from "../store/main/actions";

class AlertPopout extends Component {
    render() {
        return (
            <Alert
                actions={[{
                    title: 'Закрыть',
                    autoclose: true,
                    style: 'destructive'
                }]}
                onClose={() => this.props.dispatch(closePopout())}
            >
                {this.props.title!=='' ? <h3>{this.props.title}</h3>:null}
                {this.props.subtitle!=='' ? <p>{this.props.subtitle}</p>:null}
            </Alert>
        );
    }
}

function mapStateToProps(state) {
    return {
    };
}

export default connect(mapStateToProps)(AlertPopout);
