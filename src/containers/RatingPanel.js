import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Panel, PanelHeader, Spinner, Tabs, TabsItem, Placeholder, Button } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import * as mainSelectors from "../store/main/reducer";
import * as mainActions from '../store/main/actions';
import * as ratingSelectors from "../store/rating/reducer";
import * as ratingActions from '../store/rating/actions';
import * as vkSelectors from "../store/vk/reducer";
import * as vkActions from '../store/vk/actions';
import RatingList from './RatingList'

import Icon56UsersOutline from '@vkontakte/icons/dist/56/users_outline';

class RatingPanel extends Component {
    render() {
        return (
            <Panel id={this.props.id} separator={false}>
                <PanelHeader>
                    Рейтинг
                </PanelHeader>
                <Tabs>
                    <TabsItem
                        onClick={() => this.props.setActiveTab('users')}
                        selected={this.props.activeTab === 'users'}
                    >
                        Общий
                    </TabsItem>
                    <TabsItem
                        onClick={() => this.props.setActiveTab('friends')}
                        selected={this.props.activeTab === 'friends'}
                    >
                        Друзья
                    </TabsItem>
                </Tabs>
                {!this.props.isInit ?
                    <div className='Spinner__margin'><Spinner size="regular" /></div>
                    : <>
                        {this.props.activeTab === 'users' && <RatingList data={this.props.users} getUserOpen={this.props.getUserOpen} />}
                        {this.props.activeTab === 'friends' && (!this.props.friendsIsInit ?
                            (!this.props.tokenSettings.includes('friends') ? <Placeholder
                                icon={<Icon56UsersOutline />}
                                action={<Button size="l" mode="tertiary" onClick={() => this.props.getFriendsList()}>Получить список друзей</Button>}
                            >
                                Разрешите доступ к списку своих друзей
                            </Placeholder> :
                                <div className='Spinner__margin'><Spinner size="regular" /></div>) :
                            <RatingList data={this.props.friends} type='friends' getUserOpen={this.props.getUserOpen} />)}
                    </>
                }
                {this.props.snackbar}
            </Panel>
        );
    }
}

function mapStateToProps(state) {
    return {
        isInit: ratingSelectors.getIsInit(state),
        friendsIsInit: ratingSelectors.getFriendsIsInit(state),
        activeTab: ratingSelectors.getActiveTab(state),
        users: ratingSelectors.getUsers(state),
        friends: ratingSelectors.getFriends(state),
        friendsLoaded: ratingSelectors.getFriendsLoaded(state),
        snackbar: mainSelectors.getSnackbar(state),
        tokenSettings: vkSelectors.getTokenSettings(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setActiveTab: (value) => dispatch(ratingActions.setActiveTab(value)),
        getFriendsList: (scope) => dispatch(vkActions.getFriendsList(scope)),
        getUserOpen: (params) => dispatch(mainActions.getUserOpen(params)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RatingPanel);