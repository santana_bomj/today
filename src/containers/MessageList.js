import React from 'react';
import { Group, CellButton, Div } from '@vkontakte/vkui';


function MessageList(props) {
        let messageElements
        if (props.data.length !== 0) {
                messageElements = props.data.map(item => {
                        return <Group key={item._id}>
                                <Div>{item.text}</Div>
                                <CellButton onClick={() => props.closeMessage(item._id)} mode="danger">Закрыть</CellButton>
                        </Group>
                })
        }
        return (
                <>
                        {messageElements}
                </>
        )

}

export default MessageList;