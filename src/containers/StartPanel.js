import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Panel, FixedLayout } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';

class StartPanel extends Component {
    render() {
        return (
            <Panel id={this.props.id} separator={false}>                
                <FixedLayout className='FiexedLayout__Start' vertical="bottom"><div className='Start__title'>Я сегодня</div></FixedLayout>
            </Panel>
        );
    }
}

function mapStateToProps(state) {
    return {
    };
}

function mapDispatchToProps(dispatch) {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StartPanel);
