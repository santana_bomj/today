import * as mainActions from './store/main/actions';
import * as ratingActions from './store/rating/actions';

import * as todayActions from './store/today/actions';

export default [
    { name: 'start', path: '/?s' },
    {
        name: 'main', path: '/main', onActivate: dispatch => params => {
            dispatch(todayActions.loadGradients())
        }
    },
    {
        name: 'profile', path: '/profile', defaultParams: { id: null }, onActivate: dispatch => params => {
            dispatch(mainActions.getUser(params))
        }
    },
    {
        name: 'profileuser', path: '/profile/:id', onActivate: dispatch => params => {
            dispatch(mainActions.getUser(params))
        }
    },
    {
        name: 'rating', path: '/rating', onActivate: dispatch => params => {
            dispatch(ratingActions.openRating())
        }
    },
]