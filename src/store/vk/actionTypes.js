export const VK_GET_ACCESS_TOKEN_FETCHED = 'vk.accessTokenFetched';
export const VK_GET_ACCESS_TOKEN_FAILED = 'vk.accessTokenFailed';
export const VK_NOTIFICATION_STATUS_FETCHED = 'vk.notificationsStatusFetched';
export const VK_NOTIFICATION_STATUS_FAILED = 'vk.notificationsStatusFailed';
export const VK_INSETS_FETCHED = 'vk.insetsFetched';
export const VK_SET_TOKEN_SETTINGS = 'vk.VK_SET_TOKEN_SETTINGS';
export const VK_SET_TOKEN = 'vk.VK_SET_TOKEN';