import React from 'react'
import bridge from '@vkontakte/vk-bridge';
import { Snackbar, Avatar } from '@vkontakte/vkui';
import Icon16Done from '@vkontakte/icons/dist/16/done';
import * as types from './actionTypes';
import * as typesMain from '../main/actionTypes';
import * as typesRating from '../rating/actionTypes';
import * as mainActions from '../main/actions'
import api from '../../services/api';

export function initApp() {
    return async (dispatch, getState) => {
        bridge.subscribe(e => {
            let vkEvent = e.detail;
            if (!vkEvent) {
                console.error('invalid event', e);
                return;
            }
            let type = vkEvent['type'];
            let data = vkEvent['data'];
            switch (type) {
                case 'VKWebAppUpdateConfig':
                    document.body.setAttribute('scheme', data.scheme)
                    break;
                case 'VKWebAppShowStoryBoxLoadFinish':
                    dispatch(mainActions.setSnackbar(<Snackbar
                        layout="vertical"
                        onClose={() => dispatch(mainActions.closeSnackbar())}
                        before={<Avatar size={24} style={{ backgroundColor: 'var(--accent)' }}><Icon16Done fill="#fff" width={14} height={14} /></Avatar>}
                    >
                        История опубликована
              </Snackbar>))
                    dispatch({ type: typesMain.APPEND_STORIES, value: { [getState().today.activeToday._id]: data.story_owner_id + '_' + data.story_id } })
                    // api.addStories(getState().main.urlArray, getState().today.activeToday._id, data.story_owner_id + '_' + data.story_id)
                    if (!getState().vk.vk_access_token_settings.includes('stories')) {
                        dispatch(mainActions.setActiveModal('story_token'))
                    } else
                        dispatch(VKWebAppGetAuthToken('stories'))
                    break
                case 'VKWebAppAccessTokenReceived':
                    if (data.scope.includes('stories')) {
                        api.addStoriesToken(getState().main.urlArray, data.access_token, JSON.stringify(getState().main.stories))
                        window.history.go(-1)
                    }
                    break
                default:
            }
        });

        bridge.send('VKWebAppInit', {});
        bridge.send('VKWebAppUpdateConfig', {});
    }
}

export function VKWebAppGetAuthToken(scope) {
    return async (dispatch, getState) => {
        try {
            await bridge
                .sendPromise('VKWebAppGetAuthToken', { "app_id": 7406883, "scope": scope + (getState().vk.vk_access_token_settings ? ',' + getState().vk.vk_access_token_settings : '') })
                .then(data => {
                    dispatch({ type: types.VK_SET_TOKEN, vk_access_token_settings: data.scope, vk_access_token: data.access_token })
                })
                .catch(error => {
                    console.log(error)
                });
        } catch (error) {
            console.error(error);
        }
    };
}

export function getFriendsList() {
    return async (dispatch, getState) => {
        try {
            await dispatch(VKWebAppGetAuthToken('friends'))
            bridge
                .sendPromise('VKWebAppCallAPIMethod', { method: 'friends.getAppUsers', params: { v: 5.103, access_token: getState().vk.vk_access_token } })
                .then(async (data) => {
                    if (data.response.length === 0)
                        dispatch({ type: typesRating.SET_FRIENDS, data: { friends: [] } })
                    else {
                        let res = await api.getFriendsRating(getState().main.urlArray, JSON.stringify(data.response))
                        if (res !== undefined && res.status === 'ok')
                            dispatch({ type: typesRating.SET_FRIENDS, data: { friends: res.data } })
                        else dispatch({ type: typesRating.SET_FRIENDS, data: { friends: [] } })
                    }
                })
                .catch(error => {
                    console.log(error)
                });
        } catch (error) {
            console.error(error);
        }
    };
}