import Immutable from 'seamless-immutable';
import * as types from './actionTypes';

const initialState = Immutable({
    loaded: false,
    url: window.location.search.slice(1),
    urlArray: [],
    popout: null,
    snackbar: null,
    activeModal: null,
    modalHistory: [],
    msg_error: '',
    balance: 0,
    cards: 0,
    active_user_loading: false,
    active_user: null,
    new: false,
    avatar: '',
    canvas: null,
    canvasLoaded: false,
    authorRating: 0,
    performedCards: [],
    canvasType: '',
    name: '',
    actions: 0,
    stories: {},
    _id: null,
    user: null,
});

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.SET_POPOUT:
            return state.merge({
                popout: action.value
            });
        case types.CLOSE_POPOUT:
            return state.merge({
                popout: null
            });
        case types.SET_SNACKBAR:
            return state.merge({
                snackbar: action.value
            });
        case types.CLOSE_SNACKBAR:
            return state.merge({
                snackbar: null
            });
        case types.SET_ACTIVE_MODAL:
            if (action.modalHistory === undefined)
                action.modalHistory = []
            return state.merge({
                activeModal: action.activeModal,
                modalHistory: action.modalHistory,
                msg_error: action.msg,
            });
        case types.SET_MSG:
            return state.merge({
                activeModal: action.true ? 'true' : 'error',
                msg_error: action.msg,
            });
        case types.SET_URL:
            return state.merge({
                urlArray: action.urlArray,
                url: action.url
            });
        case types.DATA_LOADED:
            return state.merge({
                loaded: true,
                popout: null
            })
        case types.SET_NEW_USER:
            return state.merge({
                new: true
            })
        case types.SET_ACTIVE_USER_LOADIND:
            return state.merge({
                active_user_loading: action.value
            })
        case types.SET_ACTIVE_USER:
            return state.merge({
                active_user: action.data,
                active_user_loading: false,
            })
        case types.SET_AVATAR:
            return state.merge({
                avatar: action.value
            })
        case types.SET_CANVAS:
            return state.merge({
                canvas: action.value,
                canvasLoaded: true,
            })
        case types.SET_DATA:
            return state.merge({
                cards: action.data.cards,
                balance: action.data.balance,
                authorRating: action.data.authorRating,
                actions: action.data.actions
            })
        case types.SET_PERFORMED_CARDS:
            return state.merge({
                performedCards: action.data
            })
        case types.SET_CANVAS_TYPE:
            return state.merge({
                canvasType: action.value
            })
        case types.SET_NAME:
            return state.merge({
                name: action.value
            })
        case types.APPEND_STORIES:
            return state.merge({
                stories: { ...state.stories, ...action.value }
            })
        case types.CLEAR_CANVAS:
            return state.merge({
                canvas: null
            })
        case types.SET_USER_ID:
            return state.merge({
                _id: action._id
            })
        case types.SET_USER:
            return state.merge({
                user: action.data
            })
        default:
            return state;
    }
}

export function getUrl(state) {
    return state.main.url;
}
export function getPopout(state) {
    return state.main.popout;
}
export function getSnackbar(state) {
    return state.main.snackbar;
}
export function getUrlArray(state) {
    return state.main.urlArray;
}
export function getActiveModal(state) {
    return state.main.activeModal;
}
export function getBalance(state) {
    return state.main.balance;
}
export function getMsgError(state) {
    return state.main.msg_error;
}
export function getRating(state) {
    return state.main.rating;
}
export function getCards(state) {
    return state.main.cards;
}
export function getActiveUserLoading(state) {
    return state.main.active_user_loading
}
export function getActiveUser(state) {
    return state.main.active_user
}
export function getNew(state) {
    return state.main.new
}
export function getAvatar(state) {
    return state.main.avatar
}
export function getAuthorRating(state) {
    return state.main.authorRating
}
export function getPerformedCards(state) {
    return state.main.performedCards
}
export function getName(state) {
    return state.main.name
}
export function getActions(state) {
    return state.main.actions
}
export function getUser(state) {
    return state.main.user
}