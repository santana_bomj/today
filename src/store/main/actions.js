import React from 'react';
import * as types from './actionTypes';
import * as typesToday from '../today/actionTypes';
import * as typesVk from '../vk/actionTypes';
import { actions } from 'redux-router5'
import api from '../../services/api';
import story from '../../services/story';
import bridge from '@vkontakte/vk-bridge';
import html2canvas from 'html2canvas';

import { PopoutWrapper, ModalCard } from '@vkontakte/vkui';
import Icon56ErrorOutline from '@vkontakte/icons/dist/56/error_outline';

function parseQueryString(string) {
    return string.split('&')
        .map((queryParam) => {
            let kvp = queryParam.split('=');
            return { key: kvp[0], value: kvp[1] }
        })
        .reduce((query, kvp) => {
            query[kvp.key] = kvp.value;
            return query
        }, {})
};

export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export function goMain() {
    return async (dispatch, getState) => {
        try {
            dispatch(actions.navigateTo('main', {}, { replace: true }))
        } catch (error) {
            console.error(error);
        }
    }
}

export function initalMain() {
    return async (dispatch, getState) => {
        try {
            let name = ''
            let avatar = ''
            await bridge
                .sendPromise('VKWebAppGetUserInfo')
                .then(data => {
                    name = data.first_name + ' ' + data.last_name.slice(0, 1) + '.'
                    avatar = data.photo_200
                    dispatch({ type: types.SET_AVATAR, value: avatar });
                    dispatch({ type: types.SET_NAME, value: name });
                })
                .catch(error => {
                    console.log(error)
                });
            let url = getState().main.url + '&hash=' + decodeURIComponent(decodeURIComponent(window.location.hash)) + '&user_name=' + encodeURIComponent(name) + '&avatar=' + avatar
            dispatch({ type: types.SET_URL, urlArray: parseQueryString(url), url });
            dispatch({ type: typesVk.VK_SET_TOKEN_SETTINGS, value: getState().main.urlArray.vk_access_token_settings ? decodeURIComponent(getState().main.urlArray.vk_access_token_settings) : '' });
            let data = await api.inital(getState().main.urlArray);
            if (data === undefined || data.status === 'error')
                dispatch(setPopout(<PopoutWrapper className='ModalCard__reconnect'>
                    <ModalCard
                        header='Подключение прервано'
                        caption='Обнаружены неполадки с соединением'
                        icon={<Icon56ErrorOutline />}
                        onClose={() => { }}
                        actions={[{
                            title: 'Переподключиться',
                            mode: 'primary',
                            action: () => {
                                dispatch(actions.navigateTo('start', { s: '&' + getState().main.url }, { replace: true }))
                                setTimeout(() => window.location.reload(), 1000)
                            }
                        }]}
                    >
                    </ModalCard>
                </PopoutWrapper>
                ))
            else if (data.status === 'ok') {
                dispatch({ type: typesToday.SET_DATA, data: data.data })
                dispatch({ type: types.SET_USER_ID, _id: data._id })
                dispatch(goMain())
            }
        } catch (error) {
            console.error(error);
        }
    };
}

export function setPopout(value) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_POPOUT, value: value });
        } catch (error) {
            console.error(error);
        }
    };
}

export function closePopout() {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.CLOSE_POPOUT });
        } catch (error) {
            console.error(error);
        }
    };
}

export function setSnackbar(value) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_SNACKBAR, value: value });
        } catch (error) {
            console.error(error);
        }
    };
}

export function closeSnackbar() {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.CLOSE_SNACKBAR });
        } catch (error) {
            console.error(error);
        }
    };
}

export function modalBack(value = 1) {
    return async (dispatch, getState) => {
        try {
            window.history.go(value * -1)
            dispatch(setActiveModal(getState().main.modalHistory[getState().main.modalHistory.length - 2]))
        } catch (error) {
            console.error(error);
        }
    };
}

export function setActiveModal(activeModal, msg = '') {
    return async (dispatch, getState) => {
        try {
            activeModal = activeModal || null;
            let modalHistory = getState().main.modalHistory ? [...getState().main.modalHistory] : [];

            if (activeModal === null) {
                modalHistory = [];
            } else if (modalHistory.indexOf(activeModal) !== -1) {
                modalHistory = modalHistory.splice(0, modalHistory.indexOf(activeModal) + 1);
            } else {
                modalHistory.push(activeModal);
            }
            dispatch({ type: types.SET_ACTIVE_MODAL, activeModal, modalHistory, msg });
        } catch (error) {
            console.error(error);
        }
    };
}

export function getUser(params) {
    return async (dispatch, getState) => {
        try {
            if (params && params.notReload)
                return
            dispatch({ type: types.SET_ACTIVE_USER_LOADIND, value: true });
            let data = await api.getUser(getState().main.urlArray, params && params.id ? params.id : getState().main._id);
            dispatch({ type: typesToday.SET_DATA_PROFILE, data: data.data });
            if (data.user)
                dispatch({ type: types.SET_USER, data: data.user });
            dispatch({ type: types.SET_ACTIVE_USER_LOADIND, value: false });
        } catch (error) {
            console.error(error);
        }
    };
}

export function getUserOpen(params) {
    return async (dispatch, getState) => {
        try {
            dispatch(actions.navigateTo('profileuser', params))
        } catch (error) {
            console.error(error);
        }
    };
}


export function goBack() {
    return async (dispatch, getState) => {
        try {
            if (getState().main.activeModal !== null) {
                dispatch({ type: types.SET_ACTIVE_MODAL, activeModal: null });
            }
        } catch (error) {
            console.error(error);
        }
    };
}

export function setPath() {
    return async (dispatch, getState) => {
        try {
            dispatch(actions.navigateTo(getState().router.route.name, { notReload: true }))
        } catch (error) {
            console.error(error);
        }
    };
}

export function shareBtn(item) {
    return async (dispatch, getState) => {
        try {

            dispatch({ type: types.SET_ACTIVE_MODAL, activeModal: 'share' });
            dispatch({ type: typesToday.SET_ACTIVE_TODAY, data: item });
            setTimeout(() => {
                if (document.getElementById('Today__share'))
                    html2canvas(document.getElementById('Today__share'), { backgroundColor: null, imageTimeout: 0, allowTaint: true, useCORS: true, scrollX: scrollbarWidth() === 0 ? 0 : -10, scrollY: 0, scale: 5 }).then(item => {
                        dispatch({ type: types.SET_CANVAS, value: item.toDataURL() });
                    })
            }, 400)
        } catch (error) {
            console.error(error);
        }
    };
}

export function share() {
    return async (dispatch, getState) => {
        try {
            let stories
            if (getState().main.canvas) {
                stories = story.getStory(getState().main.canvas)
                bridge.send("VKWebAppShowStoryBox", stories)
            }
        } catch (error) {
            console.error(error);
        }
    };
}

function scrollbarWidth() {
    var documentWidth = parseInt(document.documentElement.clientWidth);
    var windowsWidth = parseInt(window.innerWidth);
    var scrollbarWidth = windowsWidth - documentWidth;
    return scrollbarWidth;
}

export function clearCanvas() {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.CLEAR_CANVAS })
        } catch (error) {
            console.error(error);
        }
    };
}