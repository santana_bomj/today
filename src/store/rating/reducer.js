import Immutable from 'seamless-immutable';
import * as types from './actionTypes';

const initialState = Immutable({
    activeTab: 'users',
    isInit: false,
    friendsIsInit: false,
    friendsLoaded: false,
    users: [],
    groups: [],
    friends: [],
});

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.SET_ACTIVE_TAB:
            return state.merge({
                activeTab: action.value
            })
        case types.SET_DATA:
            return state.merge({
                isInit: true,
                users: action.data,
            });
        case types.SET_FRIENDS:
            return state.merge({
                friendsIsInit: true,
                friends: action.data.friends,
            });
        case types.SET_LOADED:
            return state.merge({
                isInit: action.data.value
            });
        default:
            return state;
    }
}
export function getActiveTab(state) {
    return state.rating.activeTab
}
export function getIsInit(state) {
    return state.rating.isInit
}
export function getFriendsIsInit(state) {
    return state.rating.friendsIsInit
}
export function getUsers(state) {
    return state.rating.users
}
export function getGroups(state) {
    return state.rating.groups
}
export function getFriends(state) {
    return state.rating.friends
}
export function getFriendsLoaded(state) {
    return state.rating.friendsLoaded
}