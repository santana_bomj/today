import * as types from './actionTypes';
import api from '../../services/api';

import * as vkActions from '../vk/actions'

export function openRating() {
    return async (dispatch, getState) => {
        try {
            if (!getState().rating.isInit) {
                let data = await api.getRating(getState().main.urlArray)
                if (data !== undefined && data.status === 'ok') {
                    dispatch({ type: types.SET_DATA, data: data.data })
                }
            }
        } catch (error) {
            console.error(error);
        }
    }
}

export function setActiveTab(value) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_ACTIVE_TAB, value })
            if (!getState().rating.friendsIsInit && value === 'friends' && getState().vk.vk_access_token_settings.includes('friends'))
                dispatch(vkActions.getFriendsList())
        } catch (error) {
            console.error(error);
        }
    }
}