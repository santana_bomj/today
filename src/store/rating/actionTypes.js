export const SET_ACTIVE_TAB = 'rating.SET_ACTIVE_TAB'
export const SET_DATA = 'rating.SET_DATA'
export const SET_LOADED = 'rating.SET_LOADED'
export const SET_FRIENDS = 'rating.SET_FRIENDS'