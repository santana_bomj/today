import * as types from './actionTypes';

const initialState = {
    isLoaded: false,
    isBottomLoaded: true,
    isAllLoaded: false,
    active: null,
    gradientsList: [],
    title: '',
    description: '',
    data: [],
    activeToday: {},
    likes: [],
    likesLoaded: false,
    comment: '',
    activeComment: null,
    comments: [],
    commentsLoaded: false,
    commentLoading: false,
    profileData: [],
    isProfileLoaded: false,
    isProfileBottomLoaded: true,
    isProfileAllLoaded: false,
};

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.APPEND_GRADIENTS:
            let temp = [...state.gradientsList, ...action.data]
            return {
                ...state,
                gradientsList: temp,
                active: state.active ? state.active : temp[0].gradient
            };
        case types.SET_ACTIVE:
            let tempGradients = []
            state.gradientsList.forEach(item => {
                if (item.gradient === action.value)
                    tempGradients.push({ ...item, active: true })
                else
                    tempGradients.push({ ...item, active: false })
            })
            return {
                ...state,
                gradientsList: tempGradients,
                active: action.value
            }
        case types.HANDLE_INPUT:
            return {
                ...state,
                [action.name]: action.value
            }
        case types.APPEND_DATA:
            let temp_data = [...state.data, ...action.data]
            return {
                ...state,
                data: temp_data,
                isLoaded: true,
                isBottomLoaded: action.data.length ? true : false,
                isAllLoaded: action.data.length ? false : true,
            }
        case types.SET_DATA:
            return {
                ...state,
                data: action.data,
                isLoaded: true,
                isAllLoaded: action.data.length < 5 ? true : false,
            }
        case types.TOGGLE_LIKE:
            let temp_like = []
            state.data.forEach(item => {
                if (item._id === action._id)
                    temp_like.push({ ...item, liked: !item.liked, likes: item.liked ? item.likes - 1 : item.likes + 1 })
                else
                    temp_like.push({ ...item })
            })
            let temp_like_profile = []
            state.profileData.forEach(item => {
                if (item._id === action._id)
                    temp_like_profile.push({ ...item, liked: !item.liked, likes: item.liked ? item.likes - 1 : item.likes + 1 })
                else
                    temp_like_profile.push({ ...item })
            })
            return {
                ...state,
                data: temp_like,
                profileData: temp_like_profile
            }
        case types.SET_LOADING:
            return {
                ...state,
                isLoaded: false
            }
        case types.CLEAR_INPUT:
            return {
                ...state,
                title: '',
                description: ''
            }
        case types.APPEND_HEAD_DATA:
            let temp_head_data = [...action.data, ...state.data]
            return {
                ...state,
                data: temp_head_data,
                isLoaded: true
            }
        case types.SET_BOTTOM_LOADING:
            return {
                ...state,
                isBottomLoaded: false
            }
        case types.SET_ACTIVE_TODAY:
            return {
                ...state,
                activeToday: action.data
            }
        case types.SET_LIKES:
            return {
                ...state,
                likes: action.data,
                likesLoaded: true,
            }
        case types.CLOSE_LIKES:
            return {
                ...state,
                likes: [],
                likesLoaded: false
            }
        case types.SET_ACTIVE_COMMENT:
            return {
                ...state,
                activeComment: action.value
            }
        case types.CLOSE_COMMENT:
            return {
                ...state,
                comments: [],
                commentsLoaded: false,
                activeComment: null,
                comment: ''
            }
        case types.SET_COMMENTS:
            return {
                ...state,
                comments: action.data,
                commentsLoaded: true,
            }
        case types.APPEND_COMMENT:
            let temp_comments = []
            state.data.forEach(item => {
                if (item._id === state.activeComment)
                    temp_comments.push({ ...item, comments: item.comments + 1 })
                else
                    temp_comments.push(item)
            })
            let temp_comments_profile = []
            state.profileData.forEach(item => {
                if (item._id === state.activeComment)
                    temp_comments_profile.push({ ...item, comments: item.comments + 1 })
                else
                    temp_comments_profile.push(item)
            })
            return {
                ...state,
                comments: [...state.comments, ...action.data],
                data: temp_comments,
                profileData: temp_comments_profile
            }
        case types.CLEAR_INPUT_COMMENT:
            return {
                ...state,
                comment: ''
            }
        case types.SET_COMMENT_LOADING:
            return {
                ...state,
                commentLoading: action.value
            }
        case types.SET_DATA_PROFILE:
            return {
                ...state,
                profileData: action.data,
                isProfileAllLoaded: action.data.length < 5 ? true : false,
                isProfileLoaded: true,
                isProfileBottomLoaded: true,
            }
        case types.APPEND_DATA_PROFILE:
            let temp_data_profile = [...state.profileData, ...action.data]
            return {
                ...state,
                profileData: temp_data_profile,
                isProfileLoaded: true,
                isProfileBottomLoaded: action.data.length ? true : false,
                isProfileAllLoaded: action.data.length ? false : true,
            }
        case types.SET_BOTTOM_LOADING_PROFILE:
            return {
                ...state,
                isProfileBottomLoaded: false
            }
        case types.DELETE_TODAY:
            return {
                ...state,
                data: state.data.filter(item => item._id !== action._id),
                profileData: state.profileData.filter(item => item._id !== action._id),
            }
        default:
            return state;
    }
}
export function getActive(state) {
    return state.today.active
}
export function getGradientsList(state) {
    return state.today.gradientsList
}
export function getTitle(state) {
    return state.today.title
}
export function getDescription(state) {
    return state.today.description
}
export function getData(state) {
    return state.today.data
}
export function isLoaded(state) {
    return state.today.isLoaded
}
export function isBottomLoaded(state) {
    return state.today.isBottomLoaded
}
export function isAllLoaded(state) {
    return state.today.isAllLoaded
}
export function getActiveToday(state) {
    return state.today.activeToday
}
export function getLikes(state) {
    return state.today.likes
}
export function isLikesLoaded(state) {
    return state.today.likesLoaded
}
export function getComment(state) {
    return state.today.comment
}
export function getComments(state) {
    return state.today.comments
}
export function isCommentsLoaded(state) {
    return state.today.commentsLoaded
}
export function getCommentLoading(state) {
    return state.today.commentLoading
}
export function getProfileData(state) {
    return state.today.profileData
}
export function isProfileLoaded(state) {
    return state.today.isProfileLoaded
}
export function isProfileBottomLoaded(state) {
    return state.today.isProfileBottomLoaded
}
export function isProfileAllLoaded(state) {
    return state.today.isProfileAllLoaded
}