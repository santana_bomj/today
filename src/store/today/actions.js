import React from 'react'
import { ScreenSpinner } from '@vkontakte/vkui';
import * as types from './actionTypes';
import * as typesMain from '../main/actionTypes';
import api from '../../services/api';
import helpers from '../../services/helpers';
import * as mainActions from '../main/actions'

export function loadGradients() {
    return async (dispatch, getState) => {
        try {
            if (getState().today.gradientsList.length === 0) {
                let gradients = helpers.generateGradients(15)
                console.log(gradients)
                dispatch({ type: types.APPEND_GRADIENTS, data: gradients });
            }
        } catch (error) {
            console.error(error);
        }
    }
}

export function appendGradients() {
    return async (dispatch, getState) => {
        try {
            let gradients = helpers.generateGradients(15)
            dispatch({ type: types.APPEND_GRADIENTS, data: gradients });
        } catch (error) {
            console.error(error);
        }
    }
}

export function selectGradient(value) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_ACTIVE, value });
        } catch (error) {
            console.error(error);
        }
    }
}

export function handleInput(event) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.HANDLE_INPUT, name: event.target.name, value: event.target.value });
        } catch (error) {
            console.error(error);
        }
    };
}

export function sendToday() {
    return async (dispatch, getState) => {
        try {
            dispatch(mainActions.setPopout(<ScreenSpinner />))
            let data = await api.sendToday(getState().main.urlArray, getState().today.title, getState().today.description, getState().today.active)
            // TODO добавить ошибку
            if (data !== undefined && data.status === 'ok') {
                dispatch({ type: types.APPEND_HEAD_DATA, data: [{ _id: data._id, likes: 0, user_name: "Я", user_avatar: getState().main.urlArray.avatar, title: getState().today.title, description: getState().today.description, background: getState().today.active }] })
                dispatch({ type: types.CLEAR_INPUT })
                dispatch(mainActions.setPopout(null))
            }
        } catch (error) {
            console.error(error);
        }
    };
}

export function toggleLike(_id) {
    return async (dispatch, getState) => {
        try {
            api.toggleLike(getState().main.urlArray, _id)
            dispatch({ type: types.TOGGLE_LIKE, _id })
        } catch (error) {
            console.error(error);
        }
    };
}

export function getData() {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_LOADING })
            let data = await api.getData(getState().main.urlArray)
            // TODO добавить ошибку
            if (data !== undefined && data.status === 'ok') {
                dispatch({ type: types.SET_DATA, data: data.data })
            }
        } catch (error) {
            console.error(error);
        }
    };
}

export function appendData() {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_BOTTOM_LOADING })
            let data = await api.getData(getState().main.urlArray, getState().today.data[getState().today.data.length - 1] ? getState().today.data[getState().today.data.length - 1].ts : null)
            if (data !== undefined && data.status === 'ok') {
                dispatch({ type: types.APPEND_DATA, data: data.data })
            }
        } catch (error) {
            console.error(error);
        }
    };
}

export function openLikes(_id) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: typesMain.SET_ACTIVE_MODAL, activeModal: 'likes' });
            let data = await api.getLike(getState().main.urlArray, _id)
            if (data !== undefined && data.status === 'ok') {
                dispatch({ type: types.SET_LIKES, data: data.data })
            }
        } catch (error) {
            console.error(error);
        }
    };
}

export function closeLikes() {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.CLOSE_LIKES })
        } catch (error) {
            console.error(error);
        }
    };
}

export function openComment(_id) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: typesMain.SET_ACTIVE_MODAL, activeModal: 'comments' });
            dispatch({ type: types.SET_ACTIVE_COMMENT, value: _id });
            let data = await api.getComments(getState().main.urlArray, _id)
            if (data !== undefined && data.status === 'ok') {
                dispatch({ type: types.SET_COMMENTS, data: data.data })
            }
        } catch (error) {
            console.error(error);
        }
    };
}

export function sendComment() {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_COMMENT_LOADING, value: true })
            let data = await api.sendComment(getState().main.urlArray, getState().today.activeComment, getState().today.comment)
            // TODO добавить ошибку
            if (data !== undefined && data.status === 'ok') {
                dispatch({ type: types.APPEND_COMMENT, data: [{ _id: data._id, message: getState().today.comment, name: getState().main.urlArray.user_name, avatar: getState().main.urlArray.avatar, ts: data.ts }] })
                dispatch({ type: types.CLEAR_INPUT_COMMENT })
            }
            dispatch({ type: types.SET_COMMENT_LOADING, value: false })
        } catch (error) {
            console.error(error);
        }
    };
}

export function closeComment() {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.CLOSE_COMMENT })
        } catch (error) {
            console.error(error);
        }
    };
}

export function appendProfileData() {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_BOTTOM_LOADING_PROFILE })
            let data = await api.getUser(getState().main.urlArray, getState().main._id, getState().today.profileData[getState().today.profileData.length - 1] ? getState().today.profileData[getState().today.profileData.length - 1].ts : null)
            if (data !== undefined && data.status === 'ok') {
                dispatch({ type: types.APPEND_DATA_PROFILE, data: data.data })
            }
        } catch (error) {
            console.error(error);
        }
    };
}

export function deleteToday(_id) {
    return async (dispatch, getState) => {
        try {
            api.deleteToday(getState().main.urlArray, _id)
            dispatch({ type: types.DELETE_TODAY, _id })
        } catch (error) {
            console.error(error);
        }
    };
}