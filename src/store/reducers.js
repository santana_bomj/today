import main from './main/reducer';
import vk from './vk/reducer';
import today from './today/reducer';
import rating from './rating/reducer';
import { createStore, applyMiddleware, combineReducers } from 'redux'
import { router5Middleware, router5Reducer } from 'redux-router5'
import thunk from 'redux-thunk'
import onActivateMiddleware from '../middleware/onActivate'
import routes from '../routes';
import { createLogger } from 'redux-logger';

export default function configureStore(router, initialState = {}) {
    const createStoreWithMiddleware = applyMiddleware(
        thunk,
        router5Middleware(router),
        onActivateMiddleware(routes),
        createLogger()
    )(createStore)

    const store = createStoreWithMiddleware(
        combineReducers({
            router: router5Reducer,
            vk,
            main,
            today,
            rating,            
        }),
        initialState
    )

    window.store = store
    return store
}