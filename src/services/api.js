let URL_PREFIX = 'https://today-server.herokuapp.com'
class api {
	async inital(url) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		return fetch(URL_PREFIX, {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async sendToday(url, title, description, background) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('title', title)
		fd.append('description', description)
		fd.append('background', background)
		return fetch(URL_PREFIX + '/add', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async toggleLike(url, _id) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/toggleLike', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getData(url, last = null) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('last', last)
		return fetch(URL_PREFIX + '/getData', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getLike(url, _id) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/getLike', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async sendComment(url, _id, message) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('message', message)
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/addComment', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getComments(url, _id) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/getComments', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async addStories(url, _id, stories) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('_id', _id)
		fd.append('stories', stories)
		return fetch(URL_PREFIX + '/addStories', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async addStoriesToken(url, token, stories) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('token', token)
		fd.append('stories', stories)
		return fetch(URL_PREFIX + '/addStoriesToken', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getUser(url, _id, last = null) {
		console.log(_id)
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('_id', _id)
		fd.append('last', last)
		return fetch(URL_PREFIX + '/getUser', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async deleteToday(url, _id) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/delete', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}



	async getShop(url) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		return fetch(URL_PREFIX + '/shop', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async toggleCard(url, _id) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/toggleCard', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async toggleFavoriteCard(url, _id) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/toggleFavoriteCard', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getFavorites(url) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		return fetch(URL_PREFIX + '/getFavorites', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async sendImage(url, file, token, _id) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		file.forEach(item => {
			fd.append('file[]', item)
		})
		fd.append('token', token)
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/upload', {
			method: 'POST',
			body: fd
		})
			.then((response) => response.text())
			.then((responseText) => {
				let resp = JSON.parse(responseText);
				return resp;
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getAuthorRating(url) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		return fetch(URL_PREFIX + '/authorRating', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async sendImageAdd(url, file, token, name, description) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('file', file)
		fd.append('token', token)
		fd.append('name', (name))
		fd.append('description', (description))
		return fetch(URL_PREFIX + '/uploadTask', {
			method: 'POST',
			body: fd
		})
			.then((response) => response.text())
			.then((responseText) => {
				let resp = JSON.parse(responseText);
				return resp;
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getAuthorWork(url) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		return fetch(URL_PREFIX + '/authorWork', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async deleteAuthorWork(url, _id) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/deleteWork', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async buyShop(url, _id) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('_id', _id)
		return fetch(URL_PREFIX + '/buy', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getCards(url, exist) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('exist', exist)
		return fetch(URL_PREFIX + '/getCards', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}

	async getRating(url) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		return fetch(URL_PREFIX + '/getRating', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
	async getFriendsRating(url, friends) {
		let fd = new FormData();
		for (var key in url) {
			fd.append(key, (url[key]));
		}
		fd.append('friends', friends)
		return fetch(URL_PREFIX + '/getFriendsRating', {
			method: 'POST',
			body: fd
		})
			.then((response) => {
				return response.json()
			})
			.catch((error) => {
				return { status: "error" };
			});
	}
}

export default new api();
