class helpers {
	hexValues = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e"];

	populate(a) {
		for (var i = 0; i < 6; i++) {
			var x = Math.round(Math.random() * 14);
			var y = this.hexValues[x];
			a += y;
		}
		return a;
	}

	generateGradients(count = 1) {
		let gradients = []
		for (var i = 0; i < count; i++) {
			let newColor1 = this.populate('#');
			let newColor2 = this.populate('#');
			let angle = Math.round(Math.random() * 360);
			let gradient = "linear-gradient(" + angle + "deg, " + newColor1 + ", " + newColor2 + ")";
			gradients.push({ _id: Math.random().toString(36).substr(2, 9), gradient })
		}
		return gradients
	}

	declOfNum(n, titles) {
		return titles[n % 10 === 1 && n % 100 !== 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2];
	}
}

export default new helpers();
