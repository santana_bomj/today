class story {

    background = [
        'https://sun9-5.userapi.com/c206620/v206620117/b50e1/iG1Cs3c2Gmk.jpg',
        'https://sun9-10.userapi.com/c206620/v206620117/b50eb/J2z3EHHoCSE.jpg',
        'https://sun9-8.userapi.com/c206620/v206620117/b50f5/5yPOniJ_rvA.jpg',
        'https://sun9-30.userapi.com/c206620/v206620117/b50ff/dNd-AXhTNkw.jpg',
        'https://sun9-71.userapi.com/c206620/v206620117/b5109/KQpzR1cczJs.jpg',
        'https://sun9-45.userapi.com/c206620/v206620117/b5113/SaFDCvhl-a8.jpg',
        'https://sun9-43.userapi.com/c206620/v206620117/b511d/lwSvgnHD-DE.jpg',
        'https://sun9-69.userapi.com/c206620/v206620117/b5127/XAYhgqkYKFI.jpg',
        'https://sun9-69.userapi.com/c206620/v206620117/b5127/XAYhgqkYKFI.jpg',
        'https://sun9-20.userapi.com/c206620/v206620117/b513b/7ydjBjH29oQ.jpg',
        'https://sun9-59.userapi.com/c206620/v206620117/b5145/woPw4332K3U.jpg'
    ]

    randomInteger(min, max) {
        let rand = min - 0.5 + Math.random() * (max - min + 1);
        return Math.round(rand);
    }

    getStory(blob) {
        return {
            "background_type": "image", // set none, wait bug fix
            "url": this.background[this.randomInteger(0, this.background.length - 1)],
            "stickers": [{
                "sticker_type": "renderable", "sticker": {
                    "content_type": "image",
                    "blob": blob,
                    "clickable_zones": [{
                        "action_type": "app",
                        "action": {
                            "app_id": 7406883
                        }
                    }],
                    "transform": {
                        "relation_width": 0.7,
                        "gravity": "center",
                        "translation_y": 0.2
                    }
                }
            }],
            "attachment": {
                "text": "learn_more",
                "type": "url",
                "url": "https://vk.com/7406883" // todo set url
            }
        }
    }
}

export default new story();
